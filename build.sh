#!/bin/bash
# Build for ARM targets.
set -ex

rm -rf neon soft

make CC=arm-linux-gnueabihf-gcc OBJDUMP=arm-linux-gnueabihf-objdump ARCH=-mfpu=neon-vfpv4
mv -f bin neon
make CC=arm-linux-gnueabi-gcc OBJDUMP=arm-linux-gnueabi-objdump ARCH=""
mv -f bin soft
