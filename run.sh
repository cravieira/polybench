#!/bin/bash
set -e

for i in $(ls bin/*.x); do
    echo "Launching: $i"
    ./$i
done
