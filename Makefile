CC = gcc
OBJDUMP = objdump

UTIL = utilities/polybench.c -I utilities
FLAGS = -O3 -D MINI_DATASET -static $(ARCH)
LIBS = -lm

# Function to find files in subfolders
subdirs=$(filter-out $1,$(sort $(dir $(wildcard $1*/))))
rfind=$(wildcard $1$2) $(foreach d,$(call subdirs,$1),$(call rfind,$d,$2))

SOURCES = $(call rfind,datamining/,*.c)
SOURCES += $(call rfind,linear-algebra/,*.c)
#SOURCES += $(call rfind,medley/,*.c)
SOURCES += $(call rfind,stencils/,*.c)

EXE := $(patsubst %.c, %.x, $(SOURCES))
DUMP := $(patsubst %.x, %.dump, $(EXE))

all: bin $(EXE) $(DUMP)

bin:
	mkdir -p bin

%.x: %.c
	$(CC) -o bin/$(notdir $@) $(UTIL) $< $(LIBS) $(FLAGS)

%.dump: %.x
	$(OBJDUMP) -D bin/$(notdir $<) > bin/$(notdir $@)

.PHONY: clean

clean:
	rm -rf bin $(EXE)
